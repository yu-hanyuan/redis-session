package com.lagou.loginproject.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;

/**
 * @author yuhanyuan
 * @date 2021-05-10 18:55
 **/
@Slf4j
public class RequestInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();
        log.info("==========>>>>sessionId:{}", session.getId());
        log.info("======SESSION::::{}", session);
        Enumeration<String> attrs = session.getAttributeNames();
        while (attrs.hasMoreElements()) {
            // 获取session键值
            String name = attrs.nextElement().toString();
            // 根据键值取session中的值
            Object value = session.getAttribute(name);
            log.info("name:{};value:{}", name, value);
        }
        log.info("===============>>>>>>当前uri：{}", request.getRequestURI());
        Object username = session.getAttribute("username");
        log.info("=================>>>>>username:{}", username);
        if (username == null) {
            // 没有登录,重定向到登录页
            log.info("未登录，请登录");
            response.sendRedirect(request.getContextPath() + "/login/toLogin");
            return false;
        } else {
            log.info("已登录，放行请求");
            // 已登录，放行
            return true;
        }
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
