package com.lagou.loginproject.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lagou.loginproject.entity.LoginUser;

/**
 * @author yuhanyuan
 * @date 2021-05-10 19:11
 **/
public interface LoginUserMapper extends BaseMapper<LoginUser> {

}
