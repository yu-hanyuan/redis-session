package com.lagou.loginproject.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.lagou.loginproject.entity.LoginUser;
import com.lagou.loginproject.mapper.LoginUserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author yuhanyuan
 * @date 2021-05-10 19:14
 **/
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class LoginUserService {
    private final LoginUserMapper loginUserMapper;

    public LoginUser findUser(String username, String password) {
        LambdaQueryWrapper<LoginUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(LoginUser::getUsername, username);
        wrapper.eq(LoginUser::getPassword, password);
        return loginUserMapper.selectOne(wrapper);
    }
}
