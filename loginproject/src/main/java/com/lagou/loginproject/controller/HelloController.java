package com.lagou.loginproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author yuhanyuan
 * @date 2021-05-10 19:22
 **/
@Controller
@RequestMapping("hello")
public class HelloController {
    @RequestMapping("/result")
    public String result() {
        return "result";
    }

}
