package com.lagou.loginproject.controller;

import com.lagou.loginproject.entity.LoginUser;
import com.lagou.loginproject.service.LoginUserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * @author yuhanyuan
 * @date 2021-05-10 17:58
 **/
@Slf4j
@Controller
@RequestMapping("login")
public class LoginController {
    private final LoginUserService loginUserService;

    public LoginController(LoginUserService loginUserService) {
        this.loginUserService = loginUserService;
    }

    @RequestMapping("toLogin")
    public String toLogin() {
        log.info("++++++++++++++跳转登录页面");
        return "login";
    }

    @RequestMapping("loginSystem")
    public String loginSystem(String username, String password, HttpSession session) {
        LoginUser user = loginUserService.findUser(username, password);
        if (user != null) {
            log.info("登录成功");
            session.setAttribute("username", username);
            return "redirect:/hello/result";
        }
        log.info("用户名密码不对，重新登录");
        return "redirect:/login/toLogin";
    }

}
