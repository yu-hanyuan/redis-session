package com.lagou.loginproject.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author yuhanyuan
 * @date 2021-05-10 18:04
 **/
@Data
public class LoginUser {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String username;
    private String password;

}
